// Less configuration
import gulp from 'gulp'
import less from 'gulp-less';

gulp.task('less', function (cb) {
    gulp
        .src('less/splittermond.less')
        .pipe(less())
        .pipe(
            gulp.dest("./")
        );
    cb();
});

gulp.task(
    'default',
    gulp.series('less', function (cb) {
        gulp.watch('less/*.less', gulp.series('less'));
        cb();
    })
);
